#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank : int{
	ONE=1,
	TWO=2,
	THREE=3,
	FOUR=4,
	FIVE=5,
	SIX=6,
	SEVEN=7,
	EIGHT=8,
	NINE=9,
	TEN=10,
	JOKER=11,
	QUEEN=12,
	KING=13,
	ACE=14
};

enum Suit {
	CLUB,
	SPADE,
	HEART,
	DIAMOND
};

string enum_to_string(Suit type) {
	switch (type) {
	case CLUB:
		return "Club";
	case SPADE:
		return "Spade";
	case HEART:
		return "Heart";
	case DIAMOND:
		return "Diamond";
	default:
		return "Invalid card";
	}
}

struct Card 
{
	Suit face;
	Rank value;
	string name;
	int deckTotal = 52;
};

int main()
{
	Card c1;
	c1.face = CLUB;
	c1.value = QUEEN;

	cout << "The Value is: " << c1.value << "\nThe Face is: " << enum_to_string(c1.face);


	_getch();
	return 0;
}